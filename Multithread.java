package tema2;


import javax.swing.*;
import java.awt.*;
import java.awt.FlowLayout;
import java.util.Arrays;


public class Multithread extends Thread{
  
    int numarCase;	//cate case sunt
    int numarClienti;	//total clienti
    int minimAsteptare;	//timp minim de asteptare
    int maximAsteptare; //timp maxim de asteptare
    int minimSosire;	//timp minim sosire
    int maximSosire;	//timp maxim sosire
    int caseDeschise;	//cate case sunt deschise la un moment dat
    int pornireCasa =0;	//flag pentru deschiderea unei case
    Casa arrayCase[];	//case de marcat
    JFrame frame;
    static JLabel[] timp;	// rezultatul timpului
    static JLabel[] clienti; //rezultatul clientilor
    JPanel panou,panou1;
    static JTextArea rezultate;		//rezultatele finale
    static int[] timpServire;	//timp servire pt fiecare casa
    static int[] clientiCase;	//clientii serviti de fiecare casa

    public Multithread (int numarClienti,int minimAsteptare,int maximAsteptare,int minimSosire,int maximSosire,int numarCase)
    {
        this.numarCase =numarCase;
        this.numarClienti =numarClienti;
        this.minimAsteptare =minimAsteptare;
        this.maximAsteptare =maximAsteptare;
        this.minimSosire =minimSosire;
        this.maximSosire =maximSosire;

        frame=new JFrame("Coada");
        frame.setSize(1800, 953);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        timp =new JLabel[numarCase];
        clienti =new JLabel[numarCase];
        int eroare=1;
        
        
        rezultate = new JTextArea(67,105);
        panou= new JPanel();
        panou1=new JPanel();

        frame.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));	//punem pe frame panourile
        frame.add(panou); //primul panou
        frame.add(panou1); //al doilea panou
        JScrollPane scrollPane = new JScrollPane(rezultate);		//textul afisat il facem sa fie de tip scrollable
        frame.add(scrollPane);
        
        
        int i=0;
        while(i!=numarCase)
        {
        	if(eroare==0){	
            	System.out.println("A aparut o eroare");
            	String error="Mesaj de eroare";
            	eroare(error);
            }
            
            timp[i]=new JLabel();
            clienti[i]=new JLabel();
            panou.add(timp[i]);
            panou1.add(clienti[i]);
            i++;
        }
        
		
        clientiCase=new int[this.numarCase];
        timpServire =new int[this.numarCase];
        arrayCase = new Casa[this.numarCase];
        
        for(int j = 0; j< this.numarCase; j++)
        {
            arrayCase[j]=new Casa(j, rezultate);	//ne cream casele
        }
        caseDeschise =0;		//initial doar arrayCase 0 e deschisa
        arrayCase[0].start();		//pornim arrayCase 0

    }
    public int eroare(String error){
    	if(error=="Eroare la citire"){
    		System.out.println("Eroare la citire");
    	}
    	if(error=="Eroare la afisare"){
    		System.out.println("Eroare la afisare");
    	}
		return 1;
    	
    }

    private int alegeCasa() { 				//returneaza indicele casei cu cei mai putini clienti
        int index=0;
        int lungime;
        try{
            int mini= (int) arrayCase[0].lungimeCoada(); 	//cautam minimul si maximul
            int maxi= (int) arrayCase[0].lungimeCoada();
            for(int i = 0; i<= caseDeschise; i++)
            {
                if ((int) arrayCase[i].lungimeCoada() < mini)
                {
                    mini=(int) arrayCase[i].lungimeCoada();
                    index=i;
                }
                else if ((int) arrayCase[i].lungimeCoada() > maxi) {
                    maxi=(int) arrayCase[i].lungimeCoada();
                }
                else if ((int) arrayCase[i].lungimeCoada() == maxi) {
                    maxi=(int) arrayCase[i].lungimeCoada();
                }
            }

            //daca sunt mai putin de doi clienti, inchidem o casa
            if ((maxi <= 4)&&(caseDeschise > 1)) {
                caseDeschise--;
                System.out.println("Casa "+(caseDeschise +1)+" s-a inchide \n");
                rezultate.append("Casa "+(caseDeschise +1)+" s-a inchide \n");
            }

            //in cazul in care minimul e mai mare decat 5, deschidem o casa noua daca putem
            if ((mini > 5)&&(caseDeschise < numarCase -1)) {
                caseDeschise++; 
                index= caseDeschise;
                System.out.println("Casa "+ caseDeschise +" s-a deschis\n");
                rezultate.append("Casa "+ caseDeschise +" s-a deschis\n");
                pornireCasa =1;
            }
        }
        catch (Exception e ) {
        	eroare("Eroare in alegeCasa");
        }
        return index;
    }

    //raportul cu datele finale
    public static void sendReport(int index, int nrClienti, int timpServire, int timpAsteptareClienti)
    {
        clientiCase[index]=nrClienti;
        Multithread.timpServire[index]=timpServire;
        //rezultatele ce se afiseaza

        timp[index].setText("Casa "+index+" a servit in "+(Multithread.timpServire[index]/60)+"."+(Multithread.timpServire[index]%60)+ " minute "+clientiCase[index]+ " clienti");
        if(clientiCase[index]!=0){
        	System.out.println("Timpul mediu de servire a fost de "+(Multithread.timpServire[index]/(60*clientiCase[index]))+"."+(Multithread.timpServire[index]%(60*clientiCase[index]))+" minute/client");
            clienti[index].setText("Timpul mediu de servire a fost de "+(Multithread.timpServire[index]/(60*clientiCase[index]))+"."+(Multithread.timpServire[index]%(60*clientiCase[index]))+" minute/client");
        }
        try
        {
            Thread.sleep(5000);
        } 
        catch (InterruptedException e) {}
    }


    public void run(){
        try
        {
            int i=0;
            int[] tsos= new int[numarClienti +2];
            int[] timpSosire=tsos;	//generam timpii de sosirea a clientilor - ora de sosire la arrayCase

            for(int j = 0; j<= numarClienti; j++)
            {
                boolean gata=false;
                int timpAux=0;

                //folosim while-ul pentru a genera tot timpul un numar care sa fie valabil pentru orice caz

                while(!gata)
                {
                    double rand=Math.random();
                    double rezRandom=rand*rand;
            
                        timpAux = 8 + (int)( Math.random() * (8));
                   
                    if ((timpAux> minimSosire ) && (timpAux< maximSosire )) {
                        gata=true;	//in momentul in care timpul e intre limitele date de utilizator este adaugat in vector
                    }
                }

                timpSosire[j]=timpAux;
            }

            
            Arrays.sort(timpSosire);
            
            timpSosire[numarClienti +1]=timpSosire[numarClienti]+5;

            for(i=1;i<numarClienti;i++){
            	int timpBlocare = (int) (minimAsteptare *60+ Math.round((Math.random()*((maximAsteptare - minimAsteptare)*60)))); //generam timpul de blocare la arrayCase - timpul in care clientul sta la arrayCase sa fie servit

                int m= alegeCasa();	//cautam arrayCase cu cei mai putini clienti

                if (pornireCasa ==1) {  
                	if(!arrayCase[caseDeschise].isAlive()) {
                    arrayCase[caseDeschise].start();
                    } 
                	pornireCasa =0;
                } 	//daca e nevoie se deschide o noua arrayCase

                    sleep (5);
                    
                arrayCase[m].addClient(new Client(i,timpSosire[i],timpBlocare));	//se adauga clientul la arrayCase cu cei mai putini clienti
            }
            
            int j=0;
            while(j<numarCase){
            	arrayCase[j].oprire();
            }
        }
        catch (InterruptedException e ) {
        	eroare("O eroare a oprit rularea programului");
        	System.out.println("O eroare a oprit rularea programului");
        	}
    }
}