package tema2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class gui {

	private JFrame frame;
	private JTextField txtNrClienti;
	private JTextField txtTimpMinimCoada;
	private JTextField txtTimpMaximCoada;
	private JTextField txtTimpMinimSosire;
	private JTextField txtTimpMaximSosire;
	private JLabel nrCase;
	private JTextField txtNrCase;
	static int nrClienti2;
    static int timpMinimCoada2;
    static int timpMaximCoada2;
    static int timpMinimSosire2;
    static int timpMaximSosire2;
    static int nrCase2;
    static Multithread thread;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gui window = new gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the application.
	 */
	public gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1302, 1155);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel nrClienti = new JLabel("Numar Clienti");
		nrClienti.setBounds(149, 135, 204, 33);
		frame.getContentPane().add(nrClienti);
		
		txtNrClienti = new JTextField();
		txtNrClienti.setBounds(552, 132, 496, 39);
		frame.getContentPane().add(txtNrClienti);
		txtNrClienti.setColumns(10);
		
		JLabel timpMinimCoada = new JLabel("Timp Minim Coada");
		timpMinimCoada.setBounds(149, 298, 264, 33);
		frame.getContentPane().add(timpMinimCoada);
		
		JLabel timpMaximCoada = new JLabel("Timp Maxim Coada");
		timpMaximCoada.setBounds(149, 463, 264, 33);
		frame.getContentPane().add(timpMaximCoada);
		
		JLabel timpMinimSosire = new JLabel("Timp Minim Sosire");
		timpMinimSosire.setBounds(149, 616, 264, 33);
		frame.getContentPane().add(timpMinimSosire);
		
		JLabel timpMaximSosire = new JLabel("Timp Maxim Sosire");
		timpMaximSosire.setBounds(149, 780, 264, 33);
		frame.getContentPane().add(timpMaximSosire);
		
		txtTimpMinimCoada = new JTextField();
		txtTimpMinimCoada.setColumns(10);
		txtTimpMinimCoada.setBounds(552, 295, 496, 39);
		frame.getContentPane().add(txtTimpMinimCoada);
		
		txtTimpMaximCoada = new JTextField();
		txtTimpMaximCoada.setColumns(10);
		txtTimpMaximCoada.setBounds(552, 460, 496, 39);
		frame.getContentPane().add(txtTimpMaximCoada);
		
		txtTimpMinimSosire = new JTextField();
		txtTimpMinimSosire.setColumns(10);
		txtTimpMinimSosire.setBounds(552, 613, 496, 39);
		frame.getContentPane().add(txtTimpMinimSosire);
		
		txtTimpMaximSosire = new JTextField();
		txtTimpMaximSosire.setColumns(10);
		txtTimpMaximSosire.setBounds(552, 777, 496, 39);
		frame.getContentPane().add(txtTimpMaximSosire);
		
		JButton START = new JButton("START");
		START.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nrClienti2 = Integer.parseInt(txtNrClienti.getText()); 
			    timpMinimCoada2 = Integer.parseInt(txtTimpMinimCoada.getText());
			    timpMaximCoada2 = Integer.parseInt(txtTimpMaximCoada.getText());
			    timpMinimSosire2 = Integer.parseInt(txtTimpMinimSosire.getText());
			    timpMaximSosire2 =Integer.parseInt(txtTimpMaximSosire.getText());
			    nrCase2 =Integer.parseInt(txtNrCase.getText());
			    trimitere();
			    
			}
		});
		START.setBounds(1099, 1026, 171, 41);
		frame.getContentPane().add(START);
		
		nrCase = new JLabel("Numar Case");
		nrCase.setBounds(149, 923, 264, 33);
		frame.getContentPane().add(nrCase);
		
		txtNrCase = new JTextField();
		txtNrCase.setColumns(10);
		txtNrCase.setBounds(552, 920, 496, 39);
		frame.getContentPane().add(txtNrCase);
	}
	
	public static void trimitere(){
		thread=new Multithread(nrClienti2, timpMinimCoada2, timpMaximCoada2, timpMinimSosire2,timpMaximSosire2,nrCase2); //pornim threadurile
        thread.start();
	}
	
}
