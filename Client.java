package tema2;

public class Client {

    public int ID;
    public int timp_asteptare;
    public int timp_sorsire;
    public int timp_total;

    public Client(int ID,int timp_sosire, int timp_asteptare){
        this.ID=ID;
        this. timp_sorsire=timp_sosire;
        this.timp_asteptare=timp_asteptare;
        this.timp_total=timp_asteptare;
    }

    public int getID(){

        return ID;
    }

    public int getTimpAsteptare(){

        return timp_asteptare;
    }

    public int getTimpSosire(){

        return timp_sorsire;
    }

    public void adaugaTimp(int time){

        timp_total+=time; //timpul total de asteptare ce se modifica pe parcurs
    }
}
