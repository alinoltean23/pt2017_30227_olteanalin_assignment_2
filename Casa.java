package tema2;

import javax.swing.*;
import java.util.Vector;

/**
 * Created by alino on 4/9/2017.
 */
public class Casa extends Thread{
    private int index; //indexul casei
    private int timpServire=0; //timpul total de servire
    private Vector clienti; //vector de clienti ce stau la coada
    private int nrClienti; //nr de clienti de la coada
    private int timpAsteptareClienti=0; //timp total e asteptare
    private int oprire=0; //variabila pentru oprire
    JTextArea text;

    public Casa(int index, JTextArea text){ //constructorul casei de marcat
        this.index=index;
        this.text=text;
        clienti = new Vector();
    }

    

    
    public void stergeClient() throws InterruptedException { //stergerea clientului care a terminat de stat la coada
        Client primulClient= (Client)clienti.elementAt(0);
        if(clienti.size()==1){
            sleep(primulClient.getTimpAsteptare()); //asteptam un timp egal cu timpul de asteptare
        }
        timpAsteptareClienti+=primulClient.timp_total;
        clienti.removeElementAt(0);//stergem primul client din lista
        text.append("Clientul cu id-ul "+primulClient.getID()+" a fost servit de casa "+index+" in "+primulClient.getTimpAsteptare()/60+" avand un total de asteptare de "+primulClient.timp_total/60+" minute\n");
    }

    public long lungimeCoada(){
        return clienti.size(); //lungimea cozii
    } 
    
    public void oprire(){
        oprire=1;  //variabila pt oprirea casei
    }
    
    public void addClient(Client client){ //adaugarea unui client nou
        clienti.addElement(client);
        timpServire+=1;
        timpServire-=1;
        timpServire+=client.getTimpAsteptare();
        nrClienti++;
        int index=1;
        while(index<clienti.size()-1){
        	
        	Client c= (Client) clienti.elementAt(index);
        	client.adaugaTimp(c.getTimpAsteptare());
        	index++;
        }
    }
    
    public void run(){
        try {
            while(oprire==0 || clienti.size()!=0){ //cat timp avem clienti la coada si oprire e pe 0 le setam timpul de asteptare
                int perioadaAsteptare;
                if(clienti.size()==0){
                    perioadaAsteptare=0;
                }
                else{
                    Client clientCurent= (Client) clienti.elementAt(0);
                    perioadaAsteptare=clientCurent.getTimpAsteptare();
                }
                    sleep(perioadaAsteptare);
                if(clienti.size()!=0){
                    stergeClient();
                }
            }
            Multithread.sendReport(index,nrClienti, timpServire,timpAsteptareClienti); //trimitem datele necesare pt multithread
        }
        catch (Exception e){
            System.out.println("exceptie in casa");
        }
    }
}
